""" This Script reads the data to train a logit model on land use changes in Europe.
1. Reads the data and splits the data into training and testing data.
2. Optional - tuning the hyperparameters of the model and dumping them into a pickel file.
3. Training the model with the selected hyperparameters.
4. Optional putting out reports on performance and shap values. 
   Both these images are stored as .png file on local storage.
   In addition, AUC values are stored and in the future Boyce Index should be stored there as well.
authors: Erik Oudejans, october 2023, (Object Vision BV) 
         Ana Klinnert, october 2023, (JRC-Sevilla)"""

from helper_functions.geospatial_functions import *
from helper_functions.sampling import *
from helper_functions.boyce_index import *
import rasterio as rio
import geopandas as gpd
from osgeo import gdal
import seaborn as sns
import matplotlib.pyplot as plt
from osgeo.gdalconst import GA_ReadOnly
from osgeo import ogr
from osgeo import osr
import os
import shap
import pandas as pd
import numpy as np
import glob
from tqdm import tqdm
import pyarrow as pa
import pyarrow.parquet as pq
import json
import argparse
from pathlib import Path
import array
import pickle
import joblib
import random
random.seed(42)
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, roc_curve, auc, confusion_matrix, roc_auc_score
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import RepeatedStratifiedKFold

def create_model(solver="liblinear", penalty="l2", l1_ratio=0.5):
    model = LogisticRegression(solver=solver, penalty=penalty, l1_ratio=l1_ratio, max_iter=500000, n_jobs=-1, verbose=1)
    return model

def load_logit_model(fn):
    model = None
    with open(fn, "rb") as f:
        model = pickle.load(f)
    
    return model

def visualize_general_boyce_auc(out_fn:str, keys:list, auc_scores:list, boyce:list):
    #keys = list(loaded_hyperparameters_dict.keys())
    #auc_scores = [loaded_hyperparameters_dict[key]['auc_score'] for key in keys]
    #boyce = [loaded_hyperparameters_dict[key]['boyce'] for key in keys]


    fig, axes = plt.subplots(1, 2, figsize=(7, 5))

    axes[0].scatter(keys, auc_scores, color='blue',marker='o', alpha=0.3)
    axes[0].axhline(y=0.7, color='gray', linestyle='--', label='Threshold at 0.7')  # Add the horizontal dotted line
    axes[0].set_xlabel('Model')
    axes[0].set_ylabel('AUC Score')
    axes[0].set_title('AUC Score')
    axes[0].set_xticks([])

    axes[1].scatter(keys, boyce, color='red',marker='o', alpha=0.3)
    axes[1].axhline(y=0.4, color='gray', linestyle='--', label='Threshold at 0.4')  # Add the horizontal dotted line
    axes[1].set_xlabel('Model')
    axes[1].set_ylabel('Boyce Score')
    axes[1].set_title('Boyce Score')
    axes[1].set_xticks([])
    plt.tight_layout()
    # Hide x-axis numbers
    plt.show()
    plt.savefig(out_fn)

def train_model(model, X_train, y_train, fn):
    print(f"Start: training model {model}")
    model.fit(X_train, y_train)
    model.partial_fit(X_train, y_train)
    with open(fn, "wb") as f:
        pickle.dump(model, f)
    print(f"Stored model in {fn}")
    return model

clum_data_dir = "D:/SourceData/CLUM/"
clum_data = pq.read_table(clum_data_dir + "/clum_calibration_data_with_sampling.parquet")
def train_logit_validate_save(class_of_interest):
    model_filename = f"D:/SourceData/CLUM/models/calibrated_logit_model_{class_of_interest}.pkl"
    
    excluded_covariates = ["popdens_perdif_10_20_nona","lu_lut_class","eea_r_3035_100_m_clc12_V18_5_land_mask", "xcoord", "ycoord"]
    covariate_names = [covariate for covariate in clum_data.column_names if covariate not in excluded_covariates]
    
    # Load data:
    indices = get_indices_of_interest(clum_data, class_of_interest,'all')

    X = getX(clum_data, indices, excluded_covariates)
    Y = getY(clum_data, indices, year = 'all')
    Y = Y.reshape(-1, 1)
    
    # Handle different assigne no change assignation to one common:
    Y = np.where(Y >= 10000, 65535, Y)
    Y = np.where(Y == -1, 65535, Y)
    
    print(np.unique(Y))
    
    # Split training and test set:
    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.35, random_state=1)
    y_train = y_train.ravel()
    y_test  = y_test.ravel()

    # Create a mask of NaN values
    nan_mask = np.isnan(X)
    Y = Y[~np.any(nan_mask, axis=1)]
    X = X[~np.any(nan_mask, axis=1)]
    print(f'Done preparing the data. Number of obs.: {len(X)} and {X.shape[1]} columns.')

    # 2. Train logit:
    # create or load model
    logit_model = None
    model_exists = os.path.isfile(model_filename)
    if model_exists:
        logit_model = joblib.load(model_filename)
    else:

        logit_model = create_model()

        # Hyper param tuning
        solvers = ['sag']
        penalty = ['l2']
        c_values = [1.0, 0.1, 0.01]
        grid = dict(C=c_values) # solver=solvers,penalty=penalty,
        cv = RepeatedStratifiedKFold(n_splits=5, n_repeats=3, random_state=1)
        grid_search = GridSearchCV(estimator=logit_model, param_grid=grid, n_jobs=3, cv=cv, scoring='accuracy',error_score=0)
        grid_result = grid_search.fit(X_train, y_train)

        # Apply found params
        logit_model.solver = solvers[0]
        logit_model.C = grid_result.best_params_["C"]
        print(f"Found params for {class_of_interest} with covariate space {X.shape}: {logit_model.solver} {logit_model.C}")

        # Train the classifier
        logit_model.fit(X_train, y_train) 
            
        # 3. Save:
        model_filename 
        joblib.dump(logit_model, model_filename)
        
        print('Done training the model.')

    # results
    start_time = time.time()
    y_pred = logit_model.predict(X_test)
    y_pred_prob = logit_model.predict_proba(X_test)[:, 1]
    recall = recall_score(y_test, y_pred, average='weighted')
    auc_score = roc_auc_score(y_test, y_pred_prob)
    print(f"class: {class_of_interest}, recall: {recall}, auc_score: {auc_score}")

    # 4.2: confusion matrix:
    cm = confusion_matrix(y_test, y_pred)
    conf_matrix_percent = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    # Create a DataFrame from the confusion matrix percentage and observation number:
    cm_df = pd.DataFrame(conf_matrix_percent, index=np.unique(y_test), columns=np.unique(y_test))

    # Plot the confusion matrix using a heatmap
    plt.figure(figsize=(7, 7))
    sns.heatmap(cm_df, annot=True, cmap='Blues', linecolor='black', linewidths=1)
    plt.title(f'Confusion Matrix')
    plt.xlabel('Predicted')
    plt.ylabel('Actual')
    plt.savefig(f'D:/SourceData/CLUM/confusion_matrix/confusion_matrix{class_of_interest}.png')
    plt.close()
    
    # boyce index
    obs = y_test # y_test, y_pred
    obs = np.where(obs == class_of_interest, 1, 0)
    #print(np.unique(obs))
    #print(loaded_rf.classes_)

    y_pred_prob = logit_model.predict_proba(X_test)[:, 0]
    fit = y_pred_prob

   

    loc = f'D:/SourceData/CLUM/boyce/boyce_index_{class_of_interest}.png'
    boyce_index = ecospat_boyce(fit, obs, nclass=0, window_w="default", res=100, plot = True, plot_save=loc)
    
    auc_score = roc_auc_score(y_test, y_pred_prob)
    test = round(boyce_index,2)
    
    
    


    # SHAP
    #explainer = shap.LinearExplainer(logit_model)
    explainer = shap.Explainer(logit_model, X_train, feature_names=covariate_names)
    shap_values = explainer.shap_values(X_train)
    plt.figure(figsize=(7, 7))
    fig = shap.summary_plot(shap_values, X_train, feature_names = covariate_names,  plot_size=0.2, title = 'Feature impact on probability of being agriculture', show=False)
    plt.savefig(f'D:/SourceData/CLUM/shap_values/shap_values{class_of_interest}.png')
    plt.close()
    
    return auc_score, boyce_index

def main():
    keys = []
    auc_scores = []
    boyce_indices = []

    for class_of_interest in [  115, 12, 13, 15, 16, 17, 1415, 143, 145,
                                147, 156, 215, 23, 25, 26, 27, 31, 315, 32,
                                34, 35, 415, 42, 43, 45, 47, 515, 53,
                                54, 615, 67, 714, 715, 73, 74, 75, 76]:
        keys.append(str(class_of_interest)) 
        auc_score, boyce_index = train_logit_validate_save(class_of_interest)
        auc_scores.append(auc_score)
        boyce_indices.append(boyce_index)

    # summary plots
    visualize_general_boyce_auc(f"D:/SourceData/CLUM/summary_boyce_auc.png", keys, auc_scores, boyce_indices)
  
if __name__ == '__main__':
    main()
