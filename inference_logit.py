## This script generate the final probability of the prediction for each LT#
import pyarrow as pa
import pyarrow.parquet as pq
from tqdm import tqdm
import numpy as np
import joblib
from helper_functions.geospatial_functions import *

batch_size = 10000
parquet_file = pq.ParquetFile('D:/SourceData/CLUM/clum_calibration_data_full.parquet')

def getCovariateAtIndices(covariate:str, data, np_type=np.float32):
    return data[covariate].to_numpy().astype(np_type)

def getX(batch):
    excluded_covariates = ['xcoord', 'ycoord', 'popdens_perdif_10_20_nona','06_LUS_LB', 'LT_0612_LB', '12_all_LUS', 'LT_1218_LB', '18_all_LUS', 'eea_r_3035_100_m_clc12_V18_5_land_mask']
    num_observations = batch.num_rows
    X_covariates = np.ndarray(shape=(num_observations,len(batch.column_names)-len(excluded_covariates)), dtype=np.float32)
    
    index = 0
    for covariate in batch.column_names:
        if covariate in excluded_covariates:
            continue
        X_covariates[:,index] = getCovariateAtIndices(covariate, batch)
        index+=1

    return X_covariates

def main():
    for class_of_interest in [  115, 12, 13, 15, 16, 17, 1415, 143, 145,
                            147, 156, 215, 23, 25, 26, 27, 31, 315, 32,
                            34, 35, 415, 42, 43, 45, 47, 515, 53,
                            54, 615, 67, 714, 715, 73, 74, 75, 76]:
        index = 0
        xcoords = np.empty([parquet_file.metadata.num_rows],np.int32)
        ycoords = np.empty([parquet_file.metadata.num_rows],np.int32)
        predictions = np.empty([parquet_file.metadata.num_rows],np.float32)
        for batch in tqdm(parquet_file.iter_batches(batch_size=batch_size)):
            chunk_df = pa.Table.from_batches([batch])
            X_batch = getX(chunk_df)
            index_increment = chunk_df.num_rows                
            # do something here
            model_file_path = f"D:/SourceData/CLUM/models/calibrated_logit_model_{class_of_interest}.pkl"
            with open(model_file_path, 'rb') as file:
                logit_model = joblib.load(file)
                
            # Prediction of probabilities:
            batch_predictions = logit_model.predict_proba(X_batch)
            xcoords[index:index+index_increment] = getCovariateAtIndices("xcoord" ,chunk_df, np.int32)
            ycoords[index:index+index_increment] = getCovariateAtIndices("ycoord" ,chunk_df, np.int32)
            predictions[index:index+index_increment] = batch_predictions[:,1]
            index += index_increment

        # Produce tiff file:
        evaluation_output_dir = f"D:/SourceData/CLUM/maps/proba/"
        template_raster = "E:/SourceData/CLUM/Cleaned/06_12/V2/BiogeoRegions2016.tif"
        tiff_fn_proba_class = f"{evaluation_output_dir}prob_{class_of_interest}.tiff"
        export_sparse_to_tiff_dataset(template_raster, tiff_fn_proba_class, xcoords, ycoords, predictions)

if __name__=="__main__":
    main()
