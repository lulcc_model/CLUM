""" This Script entails the boyce function as per Hirzel et al 2006
 authors: Erik Oudejans, November 2023, (Object Vision BV) 
          Ana Klinnert, November 2023, (JRC-Sevilla)"""


import geopandas as gpd
import rasterio as rio
import numpy as np
import pandas as pd
import pyarrow.parquet as pq
import time
from osgeo import ogr
from osgeo import osr
from rasterio.features import geometry_mask
from shapely.geometry import Point
from fiona.crs import from_epsg
import pyproj
import random
import csv
import matplotlib.pyplot as plt
from scipy.stats import spearmanr

# Boyce Index
    
def ecospat_boyce(fit, obs, nclass=0, window_w="default", res=100,
                   plot = True, plot_save= None):
    '''  
    fit: the prediction probabilities for all observations.
    obs: the prediction probabilities for either the positive or negative class.
    window_w: moving window used to calculate interval for the F ratio to be evaluated
    res: resolution of interval for the F ratio to be evaluated
    plot: indicate whether plot should be produced
    plot_save: location to save the boyce index figure
    '''
    # Function to calculate the F ratio (F=P/E):
    def boycei(interval, obs, fit):
        pi = np.sum((np.logical_and(obs >= interval[0], obs <= interval[1]))) / len(obs)
        ei = np.sum(np.logical_and(fit >= interval[0], fit <= interval[1])) / len(fit)
        return round(pi / ei, 10) if ei != 0 else np.nan
    
    # Transform obs
    obs = fit[obs==1] 

    mini = np.min(fit)
    maxi = np.max(fit)
    bin_width = (np.max(fit) - np.min(fit)) / 10
    step_size = (maxi - mini )/res
    vec_mov = np.arange(start = mini, stop = maxi+step_size, step=step_size)
    interval = np.vstack((vec_mov, vec_mov+bin_width))
    
    f = []
    for i in range(interval.shape[1]):
        f.append(boycei(interval[:,i], obs, fit))
    f = np.array(f, dtype=np.float32)
    to_keep = np.where(~np.isnan(f))[0]
    f = f[to_keep]
    
    # Return Spearman correlation:
    if len(f) < 2:
        spearman_corr = np.nan
    else:
        # Only use the loc where actually change in value occurs for proper calculation of spearman corr:
        r = np.where(f != np.append(f[1:], True))[0]
        spearman_corr, _ = spearmanr(f[r], vec_mov[to_keep][r])
    
    # Produce chart:
    HS = np.sum(interval, axis=0) / 2
    #if isinstance(nclass, int) and nclass == 0:
    #   HS[-1] -= 1
        
    HS = HS[to_keep]
        
    if plot:
        plt.figure()
        plt.plot(HS, f, color='grey', marker='o', markersize=4)
        plt.xlabel('Predicted probability')
        plt.ylabel('Predicted/Expected ratio')
        plt.savefig(plot_save)
        plt.close()
    
    print(spearman_corr)
    
    return spearman_corr
    
if __name__ == '__main__':
    obs = []
    pred = []
    with open("/eos/jeodpp/data/projects/CLUM/Scripts/clum/helper_functions/boyce_test_dataset.csv", "r") as f:
        csvreader = csv.reader(f, delimiter=";")
        header = next(csvreader)
        for row in csvreader:
            obs.append(float(row[0]))
            pred.append(float(row[1]))

    obs = np.array(obs, dtype=np.float32)
    pred = np.array(pred, dtype=np.float32)
    ecospat_boyce(pred, obs, nclass=0, window_w="default", res=100, plot = True, 
    plot_save="/eos/jeodpp/data/projects/CLUM/Scripts/randomforest/boyce/test.png")

