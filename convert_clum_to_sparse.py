""" This Script creates cleaned CLUM data. Final output is a sparse representation of valid gridcells as numpy arrays
cleaning means reprojecting and resampling if needed to 100x100m pixels
 1. intermediate tiff files with uniform extent and projection are created, tiled in blocks of 256x256 pixels
 2. biogeographic regions shapefile is rasterized as the same intermediate tiff
 3. all covariates stored in intermediate tiff files are read simultaneously per-block
 4. each block is processed using vector operations in numpy:
   - get subset of all valid pixels: land_mask and valid bio georegions
   - store subset as parquet binary data
 authors: Erik Oudejans, july 2023, (Object Vision BV) 
          Ana Klinnert, august 2023, (JRC-Sevilla)"""

from helper_functions.geospatial_functions import *
from helper_functions.sampling import *
import rasterio as rio
import geopandas as gpd
from osgeo import gdal
from osgeo.gdalconst import GA_ReadOnly
from osgeo import ogr
from osgeo import osr
import os
import numpy as np
import glob
from tqdm import tqdm
import pyarrow as pa
import pyarrow.parquet as pq
import json
import argparse
from pathlib import Path
import array
import pickle

#parser=argparse.ArgumentParser()
#parser.add_argument("-input","--input",help="specify config file",dest="input",required=True,type=str, default= '')
#args = parser.parse_args()

#test
# set required settings: resolution, crs, input file with location of files:
xres = 100
yres = -100
crs_3035 = get_spatial_reference_of_epsg_from_code(3035)

#with open(args.input) as config_file:
#    config = json.load(config_file)
with open("config_V1_06_12_18.json") as fid:
    config = json.load(fid)
covariates = list(config.keys())[3::]

print(f'Done reading the settings.')

# define functions:
def get_epsg_3035_srs():
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(3035)
    srs.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
    return srs


def get_srs_from_dataset(ds, fallback_srs):
    proj = ds.GetProjection()

    if not proj:
        return fallback_srs

    if "Geocoding information not available" in proj:
        return fallback_srs

    srs_dataset = osr.SpatialReference(wkt=proj)
    srs_dataset.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)

    if not srs_dataset:
        return fallback_srs

    return srs_dataset

crs_3035 = get_epsg_3035_srs()

def warp_covariate_to_intermediate_tif(covariate_fn, intermediate_covariate_fn, bounds):    
    ds = gdal.Open(covariate_fn, GA_ReadOnly)
    no_data_value = ds.GetRasterBand(1).GetNoDataValue()

    value_type = ds.GetRasterBand(1).DataType
    has_negative_no_data_value_for_unsigned_type = not no_data_value or (no_data_value<0 and any(value_type==uvt for uvt in [1,2,4,12]))
    if not no_data_value or has_negative_no_data_value_for_unsigned_type:
        no_data_value = get_default_no_data_value_from_type(value_type)

    srs_in = get_srs_from_dataset(ds, crs_3035)
    
    kwargs = {"format": "GTiff", "outputBounds": (bounds['minx'],bounds['miny'],bounds['maxx'],bounds['maxy']),\
                "xRes":xres, "yRes":yres, "srcSRS":srs_in, "dstSRS":crs_3035,\
                "multithread":True, "creationOptions":["TILED=YES", "TFW=YES", "COMPRESS=LZW"],
                "dstNodata":no_data_value}
    ds = gdal.Warp(intermediate_covariate_fn, ds, **kwargs)

def rasterize_vector_layer(vector_dataset_in, raster_dataset_out, biogeographic_bounds):
    '''
    Function serves to rasterize biogeographic regions shapefile using the same settings
    as the same intermediate tiff.
    '''

    srs_epsg_3035 = get_epsg_3035_srs()
    
    head,_ = os.path.split(raster_dataset_out)
    if not os.path.exists(head):
        os.makedirs(head)
        
    if not os.path.exists(raster_dataset_out):
        if not biogeographic_bounds:
            biogeographic_bounds = get_minimum_spanning_extent_from_biogeographic_regions(config['clum_data_root'] + config['biogeographic_region'])

        kwargs = {"format": "GTiff", "outputBounds": (biogeographic_bounds["minx"],biogeographic_bounds["miny"],biogeographic_bounds["maxx"],biogeographic_bounds["maxy"]),\
              "xRes":xres, "yRes":yres, "outputSRS":srs_epsg_3035,\
              "creationOptions":["TILED=YES", "TFW=YES", "COMPRESS=LZW"],\
              "attribute":"PK_UID", "outputType":gdal.GDT_Byte, "noData":get_default_no_data_value_from_type(gdal.GDT_Byte)}
        gdal.Rasterize(raster_dataset_out, vector_dataset_in, **kwargs)
    else:
        print('Shapefile already rasterized.')

def get_unicode_type_code_from_numpy_type(numpy_type):
    return np.dtype(numpy_type).char

def open_raster_dataset_using_rio_with_filecheck(raster_dataset_filename:str):
    if not os.path.exists(raster_dataset_filename):
        raise (f"Error while opening raster dataset: {raster_dataset_filename}, file does not exist.")
    return rio.open(raster_dataset_filename)

'''
Code execution-----------------------------------------------------------------------------------------------------
'''

def create_nodata_mask_from_tiff_file(tiff_file : str):
    nodata_value = None
    data = None
    with rio.open(tiff_file, 'r') as rio_dataset:
        nodata_value = rio_dataset.nodata
        data = rio_dataset.read()

    return data != nodata_value

def create_observation_mask(clum_tiff_filenames : list):
    mask = np.array([])

    landuse_06_fn = '06_LUS_LB.tif'
    lut_06_12_fn = 'LT_0612_LB.tif'
    landuse_12_fn = '12_all_LUS.tif'
    lut_12_18_fn = 'LT_1218_LB.tif'
    land_mask = 'eea_r_3035_100_m_clc12_V18_5_land_mask.tif'
    bioregion = 'BiogeoRegions2016.tif'
    cleaned_source = config['clum_data_root'] + config['data_clean']

    with rio.open(cleaned_source+land_mask) as f:
        mask = f.read() == 1

    with rio.open(cleaned_source+bioregion) as f:
        data = f.read()
        mask = np.logical_and(mask, np.logical_and(data != 10, data != f.nodata)) # exclude region 10

    with rio.open(cleaned_source+landuse_06_fn) as f1:
        with rio.open(cleaned_source+lut_06_12_fn) as f2:
            lu_06 = f1.read()
            lut_06_12 = f2.read()
            mask = np.logical_and(mask, np.logical_or(lu_06!=f1.nodata, lut_06_12!=f2.nodata))
            
    with rio.open(cleaned_source+landuse_12_fn) as f1:
        with rio.open(cleaned_source+lut_12_18_fn) as f2:
            lu_12 = f1.read()
            lut_12_18 = f2.read()
            mask = np.logical_and(mask, np.logical_or(lu_12!=f1.nodata, lut_12_18!=f2.nodata))

    for clum_tiff_file in tqdm(clum_tiff_filenames):
        if any(substring in clum_tiff_file for substring in (landuse_06_fn, lut_06_12_fn, landuse_12_fn, lut_12_18_fn, land_mask, bioregion)):
            continue

        nodata_mask = create_nodata_mask_from_tiff_file(clum_tiff_file)
        mask = np.logical_and(mask, nodata_mask)

    print(sum(mask))
    return mask

def get_unique_landuse_transition_counts(cleaned_lut_fn:str, mask):
    data = None
    unique_luts = None
    unique_lut_counts = []
    with rio.open(cleaned_lut_fn, 'r') as rio_dataset:
        data = rio_dataset.read()[mask]
        unique_luts = np.unique(data)

    unique_luts = unique_luts[unique_luts>9]
    unique_luts = unique_luts[unique_luts!=65534]

    for lut in unique_luts:
        count = np.count_nonzero(data==lut)
        unique_lut_counts.append((int(lut), count))

    return unique_lut_counts

def merge_unique_landuse_transition_counts(lut_counts_1:list, lut_counts_2:list):
    merged_counts = {}
    for lut,count in lut_counts_1:
        merged_counts[lut] = count
    for lut,count in lut_counts_2:
        if lut in merged_counts:
            merged_counts[lut] += count
        else:
            merged_counts[lut] = count

    return merged_counts

def simple_random_sampling_of_stable_landuse():
    pass

def main():
    # params
    random_seed = 42
    sampling_factor = 1.0
    
    # gather filenames
    landuse_06_fn = '06_LUS_LB.tif'
    lut_06_12_fn = 'LT_0612_LB.tif'
    landuse_12_fn = '12_all_LUS.tif'
    lut_12_18_fn = 'LT_1218_LB.tif'
    landuse_18_fn = '18_all_LUS.tif'
    land_mask = 'eea_r_3035_100_m_clc12_V18_5_land_mask.tif'
    bioregion = 'BiogeoRegions2016.tif'
    cleaned_source = config['clum_data_root'] + config['data_clean']
    temp_out_mask_fn  = cleaned_source + "mask.bin"

    np.random.seed(seed=42)

    clum_tiff_filenames = [cleaned_source+landuse_06_fn, cleaned_source+lut_06_12_fn, cleaned_source+landuse_12_fn, cleaned_source+lut_12_18_fn, cleaned_source+land_mask, cleaned_source+bioregion]
    tiff_files = glob.glob(cleaned_source + '*.tif')
    for covariate_file in tiff_files:
        if any( substring in covariate_file for substring in (landuse_06_fn, lut_06_12_fn, landuse_12_fn, lut_12_18_fn, land_mask, bioregion)):
            continue

        clum_tiff_filenames.append(covariate_file)

    pq_out_fn = cleaned_source + "clum_calibration_data_with_sampling.parquet"
    pq_out_full_fn = cleaned_source + "clum_calibration_data_full.parquet"

    biogeographic_bounds = None
     
    # 1. Warp covariates in loop:
    for covariate in covariates:
        covariate = config[covariate]
        fn_raw = os.path.split(covariate)[1]
        fn = os.path.splitext(fn_raw)[0] + ".tif"
        intermediate_covariate_fn = config['clum_data_root']+config['data_clean']+fn
        print(intermediate_covariate_fn)
        if os.path.exists(intermediate_covariate_fn):
            continue
        if not biogeographic_bounds:
           biogeographic_bounds = get_minimum_spanning_extent_from_biogeographic_regions(config['clum_data_root'] + config['biogeographic_region'])
        head, tail = os.path.split(intermediate_covariate_fn)
        if not os.path.exists(head):
            os.makedirs(head)

        warp_covariate_to_intermediate_tif(config['clum_data_root']+covariate, intermediate_covariate_fn, biogeographic_bounds)
    
    print('Done warping tif files to same extent, resolution, srs.')
    
    # 2. rasterize biogeographic regions
    rasterize_vector_layer(config['clum_data_root'] + config['biogeographic_region'], 
                       config['clum_data_root'] + config['data_clean'] + "BiogeoRegions2016.tif", biogeographic_bounds) 
    print('Done with converting biogeographic regions to raster.')
    

    # 3. create observation mask
    obs_mask = None
    if not os.path.exists(temp_out_mask_fn):
        obs_mask = create_observation_mask(clum_tiff_filenames)
        with open(temp_out_mask_fn, 'wb') as f:
            pickle.dump(obs_mask, f)
    else:
        with open(temp_out_mask_fn, 'rb') as f:
            obs_mask = pickle.load(f)
            test_mask_fn = cleaned_source + "testmask.tiff"
            template_raster_fn = cleaned_source + land_mask
            if not os.path.exists(test_mask_fn):
                with rio.open(template_raster_fn, "r") as src:
                    metainfo = src.profile
                    bounds = src.bounds

                metainfo['tiled']       = True
                metainfo['blockxsize']  = 256
                metainfo['blockysize']  = 256
                metainfo['compress']    = 'lzw'
                metainfo['num_threads'] = 'all_cpus'

                with rio.open(test_mask_fn, 'w', **metainfo) as dst:
                    test_shape = obs_mask.shape
                    dst.write_band(1,np.squeeze(obs_mask))

    print(f"obs_mask: {np.count_nonzero(obs_mask)}")
    unique_landuse_transition_counts_06_12 = get_unique_landuse_transition_counts(cleaned_source+lut_06_12_fn, obs_mask)
    unique_landuse_transition_counts_12_18 = get_unique_landuse_transition_counts(cleaned_source+lut_12_18_fn, obs_mask)

    merged_landuse_transition_counts = merge_unique_landuse_transition_counts(unique_landuse_transition_counts_06_12, unique_landuse_transition_counts_12_18)
    lut_lookup_table = create_landtransition_lookup_table("C:/Users/Cicada/prj/clum/landuse_transition_codes_table.txt")

    # write observations to parquet file
    lu_06     = None
    with rio.open(cleaned_source+landuse_06_fn) as f:
        lu_06 = f.read()
    bounds = f.bounds
    height = lu_06.shape[1]
    width  = lu_06.shape[2]
    lu_12     = None
    with rio.open(cleaned_source+landuse_12_fn) as f:
        lu_12 = f.read()
    
    lut_06_12 = None
    lut_06_12_nodata = None
    with rio.open(cleaned_source+lut_06_12_fn) as f:
        lut_06_12 = f.read()
        lut_06_12_nodata = f.nodata

    lut_12_18 = None
    lut_12_18_nodata = None
    with rio.open(cleaned_source+lut_12_18_fn) as f:
        lut_12_18 = f.read()
        lut_12_18_nodata = f.nodata

    table_data = {}
    table_data_full = {}
    index = 0
    observation_indices = np.array([])
    lut_lu_ids = np.array([], np.uint32)
    print("Create observation (lut + random sample) indices per lut")
    for lut in tqdm(merged_landuse_transition_counts.keys()):
        if not lut in [ 115, 12, 13, 15, 16, 17, 1415, 143, 145,
                        147, 156, 215, 23, 25, 26, 27, 31, 315, 32,
                        33, 34, 35, 415, 42, 43, 44, 45, 47, 515, 53,
                        54, 615, 67, 714, 715, 73, 74, 75, 76, 77]:
            print(f"skipping irrelevant lut: {lut}")
            continue
        
        print(f"processing lut {lut}")
        count = merged_landuse_transition_counts[lut]
        lu_from = int(lut_lookup_table[lut][0])
        lu_to   = int(lut_lookup_table[lut][1])

        if lu_from == 0:
            continue

        if lu_to == 0:
            continue

        valid_lut_indices     = np.logical_and(obs_mask, np.logical_or(lut_06_12==lut, lut_12_18==lut))
        valid_non_lut_indices = np.logical_and(lut_06_12==lut_06_12_nodata, np.logical_or(lut_12_18==lut_12_18_nodata, lut_12_18==65534))
        valid_non_lut_indices = np.logical_and(valid_non_lut_indices, obs_mask)
        valid_non_lut_indices = np.logical_and(valid_non_lut_indices, np.logical_or(lu_06==lu_from, lu_12==lu_from))

        # simple random sample
        number_valid_lu_indices = np.count_nonzero(valid_non_lut_indices)
        if not number_valid_lu_indices:
            continue

        if count > number_valid_lu_indices:
            count = number_valid_lu_indices

        non_zero_lu_indices = np.nonzero(valid_non_lut_indices.flatten())
        random_indices = np.random.randint(0, number_valid_lu_indices, count, dtype=int)
        random_sample_indices = np.take(non_zero_lu_indices, random_indices)

        valid_lut_indices = np.nonzero(valid_lut_indices.flatten())

        # add observation indices
        if observation_indices.size == 0:
            observation_indices = np.append(random_sample_indices, valid_lut_indices)
        else:
            observation_indices = np.append(observation_indices, random_sample_indices)
            observation_indices = np.append(observation_indices, valid_lut_indices)

        lut_lu_ids = np.append(lut_lu_ids, [np.full((count, 1), lu_from*10000), np.full((valid_lut_indices[0].size, 1), lut)])


    # cleanup unnecessary memory
    lu_06 = None
    lu_12 = None
    lu_18 = None
    lut_06_12 = None
    lut_12_18 = None

    # x, y coordinates
    xcoord_row = np.arange(bounds[0]+xres/2, bounds[2]+xres/2, xres, np.int32) # create a x-coordinate row given window bounds and x,y res
    ycoord_row = np.arange(bounds[3]+yres/2, bounds[1]+yres/2, yres, np.int32)
    xcoord_block = np.tile(xcoord_row, (height,1))
    ycoord_block = np.tile(ycoord_row, (width,1)).transpose()

    table_data['xcoord'] = xcoord_block.flatten()[observation_indices]
    table_data['ycoord'] = ycoord_block.flatten()[observation_indices]
    table_data_full['xcoord'] = xcoord_block.squeeze()[obs_mask.squeeze()]
    table_data_full['ycoord'] = ycoord_block.squeeze()[obs_mask.squeeze()]

    xcoord_row = None
    ycoord_row = None
    xcoord_block = None
    ycoord_block = None

    # fill [lu_lut_class]
    table_data['lu_lut_class'] = lut_lu_ids

    # fill test set
    for fn in tqdm([landuse_06_fn, lut_06_12_fn, landuse_12_fn, lut_12_18_fn, landuse_18_fn, land_mask]):
        with rio.open(cleaned_source+fn) as f:
            data = f.read()
            var_name = Path(fn).stem
            table_data_full[var_name] = data[obs_mask]

    lut_lu_ids = None

    # add per covariate
    for covariate_file in tqdm(clum_tiff_filenames):
        if any(substring in covariate_file for substring in (landuse_06_fn, lut_06_12_fn, landuse_12_fn, lut_12_18_fn, landuse_18_fn, land_mask)):
            continue

        with rio.open(covariate_file) as f:
            data = f.read()
            observations = data.flatten()[observation_indices]
            var_name = Path(covariate_file).stem
            table_data[var_name] = observations
            table_data_full[var_name] = data[obs_mask]

    table = pa.Table.from_pydict(table_data)
    table_data = None

    table_full = pa.Table.from_pydict(table_data_full)
    table_data_full = None

    pq.write_table(table, pq_out_fn, use_dictionary=True, compression='snappy')
    pq.write_table(table_full, pq_out_full_fn, use_dictionary=True, compression='snappy')

    return
    # 3. all covariates stored in intermediate tiff files are read simultaneously per-block


    # mask files
    landuse_12_fn = '12_all_LUS.tif'
    landuse_18_fn = '18_all_LUS.tif'
    lut_12_18_fn = 'LT_1218_LB.tif'
    land_mask = 'eea_r_3035_100_m_clc12_V18_5_land_mask.tif'
    bioregion = 'BiogeoRegions2016.tif'

    cleaned_source = config['clum_data_root'] + config['data_clean']


    landuse_12_ds = open_raster_dataset_using_rio_with_filecheck(cleaned_source + landuse_12_fn)
    #landuse_18_ds = open_raster_dataset_using_rio_with_filecheck(cleaned_source + landuse_18_fn)
    lut_12_18_ds = open_raster_dataset_using_rio_with_filecheck(cleaned_source + lut_12_18_fn)
    land_mask_ds = open_raster_dataset_using_rio_with_filecheck(cleaned_source + land_mask)
    bioregion_ds = open_raster_dataset_using_rio_with_filecheck(cleaned_source + bioregion)

    # open all datasets
    pq_out_fn = cleaned_source + "clum_calibration_data.parquet"
    #out_names = ["lu12", "lu18", "lut1218", "biogeo", "xcoord", "ycoord"]
    out_names = ["lu12", "lut1218", "biogeo", "xcoord", "ycoord"]
    datasets = []
    dataset_types = [landuse_12_ds.dtypes[0], lut_12_18_ds.dtypes[0], bioregion_ds.dtypes[0], np.int32, np.int32]
    #dataset_types = [landuse_12_ds.dtypes[0], landuse_18_ds.dtypes[0], lut_12_18_ds.dtypes[0], bioregion_ds.dtypes[0], np.int32, np.int32]
    #sparse_data = [[],[],[],[],[]]
    sparse_data = []
    for i, t in enumerate(dataset_types):
        sparse_data.append(array.array(get_unicode_type_code_from_numpy_type(dataset_types[i]),[]))

    tiff_files = glob.glob(config['clum_data_root'] + config['data_clean'] +'*.tif')
    for covariate_file in tiff_files:
        #if any( substring in covariate_file for substring in (landuse_12_fn, landuse_18_fn, lut_12_18_fn, land_mask, bioregion)):
        if any( substring in covariate_file for substring in (landuse_12_fn, lut_12_18_fn, land_mask, bioregion)):
            continue
        datasets.append(open_raster_dataset_using_rio_with_filecheck(covariate_file))
        dataset_type = datasets[-1].dtypes[0]
        dataset_types.append(dataset_type)
        sparse_data.append(array.array(get_unicode_type_code_from_numpy_type(dataset_type),[]))
        var_name = Path(covariate_file).stem #[key for key, value in config.items() if covariate_file.split('/')[-1].replace('.tif', '') in value][0]
        out_names.append(var_name)


    # TODO: precreate binary mask grid of Europe
    

    # process all datasets block-by-block
    # tqdm provide a progress bar for the loop, which can be helpful for tracking the progress of the loop
    # .block_window(1) returns a generator of subsequent windows of the 1st band (in case there a multiple bands better to specify)
    for _, window in tqdm(datasets[0].block_windows(1)):
        landuse_12_block = landuse_12_ds.read(1, window=window)
        #landuse_18_block = landuse_18_ds.read(1, window=window)
        lut_12_18_block = lut_12_18_ds.read(1, window=window)
        land_mask_block = land_mask_ds.read(1, window=window)
        bioregion_block = bioregion_ds.read(1, window=window)

        # geographic bound block 
        bounds = rio.windows.bounds(window, bioregion_ds.transform)
        xcoord_row = np.arange(bounds[0]+xres/2, bounds[2]+xres/2, xres, np.int32) # create a x-coordinate row given window bounds and x,y res
        ycoord_row = np.arange(bounds[3]+yres/2, bounds[1]+yres/2, yres, np.int32)
        xcoord_block = np.tile(xcoord_row, (window.height,1))
        ycoord_block = np.tile(ycoord_row, (window.width,1)).transpose()

        # compute observation mask
        lu_12_mask     = landuse_12_block != landuse_12_ds.nodata
        #lu_18_mask     = landuse_18_block != landuse_18_ds.nodata
        lut_12_18_mask = lut_12_18_block != lut_12_18_ds.nodata
        land_mask      = land_mask_block==1
        bioreg_mask    = np.logical_and(bioregion_block != 10, bioregion_block != bioregion_ds.nodata)
        #obs_mask = np.logical_and(land_mask, lut_12_18_mask)
        #obs_mask = np.logical_and(obs_mask, lu_12_mask)
        obs_12_18_mask = np.logical_and(lu_12_mask, lu_12_mask) #lu_18_mask)
        obs_mask = np.logical_and(land_mask, np.logical_or(obs_12_18_mask, lut_12_18_mask))
        obs_mask = np.logical_and(obs_mask, bioreg_mask)

        if (not np.any(obs_mask)): # empty mask, don't go over covariates
            continue

        # add covariates to observations mask and safe all data from covariates in covariates_blocks
        covariate_blocks = []
        covariate_masks = []
        for index, ds in enumerate(datasets):
            covariate_block = ds.read(1, window=window)
            covariate_blocks.append(covariate_block)
            obs_mask = np.logical_and(obs_mask, covariate_block!=ds.nodata)
            if (not np.any(obs_mask)):
                break

        if (not np.any(obs_mask)): # no observations in mask
            continue

        # TODO: add observation cells to binary mask grid of Europe
        # TODO: remove below

        # fill sparse data
        sparse_data[0].extend(landuse_12_block[obs_mask].tolist())
        #sparse_data[1].extend(landuse_18_block[obs_mask].tolist())
        sparse_data[1].extend(lut_12_18_block[obs_mask].tolist())
        sparse_data[2].extend(bioregion_block[obs_mask].tolist())
        sparse_data[3].extend(xcoord_block[obs_mask].tolist())
        sparse_data[4].extend(ycoord_block[obs_mask].tolist())

        # Loop through covariate_blocks and mask them with the obs_mask 
        for i, covariate_block in enumerate(covariate_blocks):
            sparse_data[i+5].extend(covariate_block[obs_mask].tolist())

    print('Done processing the data. Next task: writing to disk.')

    # export to parquet data
    table_data = {}
    for i, col in enumerate(sparse_data):
        table_data[out_names[i]] = np.array(col, dataset_types[i])
        sparse_data[i] = [] # free memory after copy
    table = pa.Table.from_pydict(table_data)
    pq.write_table(table, pq_out_fn, use_dictionary=True, compression='snappy')

if __name__ == '__main__':
  main()
