# CLUM

# Description

The Calibration of Land Use Model (CLUM) is a Python package that contains scripts for data preparation, model calibration, and validation.

## Scripts description

### calibration_logit.py and calibration_rf.py: 
Both these codes are used to sample, train, hypertuning and evaluating the classification algorithms. _rf refers to random forest, while _logit to the logistic regression. When running the model one can choose whether all steps (training, hypertuning, evaluating) should be run or if more of interest to run a specific part. These options can be change by changing the arguments parsed into the function. Example for executing the code from command line for a specific class: python calibration_rf.py --class 15 --tune 'False' --report 'True' --train 'False'. In this case one is only interested in reporting the performances for land use transition model 15. Side note: calculation of SHAP values under the calibration_rf.py are particularly heavy in memory consumption and require larger computation time when the land use transition has more than 100’000 observations.
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

### calibration_rf_loop.py: 
Same as calibration_rf.py but allows you to loop over several land use transitions. Consequently the –class argument is no longer required. The land use transition to be looped over need to specified in the source code, i.e. calibration_rf_loop.py. 

### inference_logit.py and inference_rf.py: 
Script used to predict probability of change for a given land use transition for all pixels in Europe with available covariate values. _rf refers to random forest, while _logit to the logistic regression. Note that these script make use of the full covariate dataset saved as clum_calibration_data_full_parquet and executes the prediction in batches of that parquet file (see in code for parquet_file.iter_batches  (batch_size = batch_size)). This is done as predicting for all Europe at the same time would exceed the given memory allocation. Example for executing the code: python inference_rf.py –class 15

### inference_rf_loop.py: 
Same as inference_rf.py but allows you to loop over several land use transitions. Consequently the –class argument is no longer required. The land use transition to be looped over need to specified in the source code, i.e. inference_rf_loop.py.

### Helper function: 
#### o	geospatial_functions.py: 
Several functions used to perform geospatial tasks.
#### sampling.py: 
Function used to sample dataset for a specific land use transition.
#### boyce_index.py: 
Function used to calculate boyce index, as no package was found to have integrated this function. The code follows closely the code written under R to calculate the boyce index from the ecospat package (see: https://github.com/cran/ecospat/blob/master/R/ecospat.boyce.R). 

### Transition_selection_Europe.ipynb: 
Notebook used to identify the land use classes to be discarded, i.e. representing less than 2% of all transitions within a specific land use class.

### convert_clum_to_sparse.py:: 
 Pipeline that takes input covariate files and landuse change and landuse maps and converts these maps into a sparse representation to be efficiently used by machine learning algorithms. See paragraph 2.1.1 for a more detailed explanation.

### LUS_12_18_status: 
Mapping CORINE Land Cover categories with the LUISA_BEES land use categories;

## Hard validation:
This notebook aims to validate the results of the LUISA-BEES Model. For this purpose we use the CLC2018 and compare with the simulated land use for the Year 2020 derived from the Logit regression model and Random Forest.

## Authors and acknowledgment
Ana Luisa Barbosa (JRC-SEVILLE - Unit D4)
Ana Klinnert (JRC-SEVILLE - Unit D4) 
Erik Oudjeans (ObjectVision) 
Maarten Hilferink (ObjectVision)
Ignacio Perez-Dominguez (JRC-SEVILLE - Unit D4)


## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/lulcc_model/CLUM.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/lulcc_model/CLUM/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***