import pyarrow as pa
import pyarrow.parquet as pq
from tqdm import tqdm
import numpy as np
import joblib
import argparse
from helper_functions.geospatial_functions import *
from helper_functions.sampling import *

batch_size = 10000
parquet_file = pq.ParquetFile('/eos/jeodpp/data/projects/CLUM/Data/Cleaned/parquet/clum_calibration_data_full.parquet')
parser=argparse.ArgumentParser()
parser.add_argument("-class","--class",help="specify tansition",dest="tr_class",type = int, required=True, default= 13)
args = parser.parse_args()


def getCovariateAtIndices(covariate:str, data, np_type=np.float32):
    return data[covariate].to_numpy().astype(np_type)

def getX(batch):
    excluded_covariates = ['xcoord', 'ycoord', 'popdens_perdif_10_20_nona','06_LUS_LB', 'LT_0612_LB', '12_all_LUS', 'LT_1218_LB', '18_all_LUS', 'eea_r_3035_100_m_clc12_V18_5_land_mask']
    num_observations = batch.num_rows
    X_covariates = np.ndarray(shape=(num_observations,len(batch.column_names)-len(excluded_covariates)), dtype=np.float32)
    
    index = 0
    for covariate in batch.column_names:
        if covariate in excluded_covariates:
            continue
        #print(f"covariate: {index} {covariate}")
        X_covariates[:,index] = getCovariateAtIndices(covariate, batch)
        index+=1

    return X_covariates

def main():
    xcoords = np.empty([parquet_file.metadata.num_rows],np.int32)
    ycoords = np.empty([parquet_file.metadata.num_rows],np.int32)
    predictions = np.empty([parquet_file.metadata.num_rows],np.float32)
    
    index = 0 
    for batch in tqdm(parquet_file.iter_batches(batch_size=batch_size)):
        
        chunk_df = pa.Table.from_batches([batch])
        X_batch = getX(chunk_df)
        index_increment = chunk_df.num_rows
                        
        # do something here
        class_of_interest = args.tr_class 
        model_file_path = f"/eos/jeodpp/data/projects/CLUM/Scripts/randomforest/model_calibration/calibrated_rf_model_{class_of_interest}.pkl"
        with open(model_file_path, 'rb') as file:
            rf = joblib.load(file)
            
        # Prediction of probabilities:
        batch_predictions = rf.predict_proba(X_batch)
        batch_x_coord = getCovariateAtIndices("xcoord" ,chunk_df, np.int32)
        batch_y_coord = getCovariateAtIndices("ycoord" ,chunk_df, np.int32)

        xcoords[index:index+index_increment] = batch_x_coord
        ycoords[index:index+index_increment] = batch_y_coord
        predictions[index:index+index_increment] = batch_predictions[:,0]
         
        index += index_increment
        
    # Produce tiff file:
    evaluation_output_dir = f"/eos/jeodpp/data/projects/CLUM/Results/RF/proba/"
    template_raster = "/eos/jeodpp/data/projects/CLUM/Data/Cleaned/Total/V3/BiogeoRegions2016.tif"

    tiff_fn_proba_class = f"{evaluation_output_dir}prob_{class_of_interest}_{0}.tiff"
    export_sparse_to_tiff_dataset(template_raster, tiff_fn_proba_class, xcoords, ycoords, predictions)

if __name__=="__main__":
    main()
