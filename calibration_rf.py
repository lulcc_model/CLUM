""" This Script reads the data to train a random forest model on land use changes in Europe.
1. Reads the data and splits the data into training and testing data.
2. Optional - tuning the hyperparameters of the model and dumping them into a pickel file.
3. Training the model with the selected hyperparameters.
4. Optional putting out reports on performance and shap values. 
   Both these images are stored as .png file on local storage.
   In addition, AUC values are stored and in the future Boyce Index should be stored there as well.
authors: Erik Oudejans, october 2023, (Object Vision BV) 
         Ana Klinnert, october 2023, (JRC-Sevilla)"""

from helper_functions.geospatial_functions import *
from helper_functions.sampling import *
from helper_functions.boyce_index import *
import rasterio as rio
import geopandas as gpd
import seaborn as sns
import matplotlib.pyplot as plt
from osgeo.gdalconst import GA_ReadOnly
from osgeo import ogr
from osgeo import osr
import os
import shap
import pandas as pd
import numpy as np
import glob
from tqdm import tqdm
import pyarrow as pa
import pyarrow.parquet as pq
import json
import argparse
from pathlib import Path
import array
import pickle
import joblib
import random
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, roc_curve, auc, confusion_matrix, roc_auc_score

# Load arguments provided in command execution:
parser=argparse.ArgumentParser()
parser.add_argument("-class","--class",help="specify tansition",dest="tr_class",type = int, required=True, default= 13)
parser.add_argument("-train","--train",help="training",dest="train", required=True, default= 'True')
parser.add_argument("-tune","--tune",help="hypertune",dest="tune",required=False, default= 'False')
parser.add_argument("-report","--report",help="produce performance",dest="report",required=True, default= 'False')
args = parser.parse_args()

def train_rf_validate_save():
    # Define parameters
    class_of_interest = args.tr_class # 3 or -1 for all classes
    tune = args.tune
    biogeographic_region_of_interest = -1 
    train = args.train
    report = args.report
    
    clum_data_dir = "/eos/jeodpp/data/projects/CLUM/Data/"
    clum_data = pq.read_table(clum_data_dir + "Cleaned/parquet/clum_calibration_data_with_sampling.parquet")
    
    excluded_covariates = ["popdens_perdif_10_20_nona","lu_lut_class","eea_r_3035_100_m_clc12_V18_5_land_mask", "xcoord", "ycoord"]
    covariate_names = [covariate for covariate in clum_data.column_names if covariate not in excluded_covariates]
    
    # Load data:
    indices = get_indices_of_interest(clum_data, class_of_interest,'all')

    X = getX(clum_data, indices, excluded_covariates)
    Y = getY(clum_data, indices, year = 'all')

    X = X
    Y = Y.reshape(-1, 1)
    
    # Handle different assigne no change assignation to one common:
    Y = np.where(Y >= 10000, 65535, Y)
    Y = np.where(Y == -1, 65535, Y)
    
    print(np.unique(Y))
    
    # Split training and test set:
    random.seed(42)
    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.35, random_state=1)
    y_train = y_train.ravel()
    y_test  = y_test.ravel()

    df_X_test= pd.DataFrame(X_test)
    df_X_test.to_excel('/eos/jeodpp/data/projects/CLUM/Scripts/randomforest/boyce/testdata_Xtest1.xlsx')


    #test_indices = np.where(np.isin(X, X_test).all(axis=1))[0]
    #print(test_indices)

    # Create a mask of NaN values
    nan_mask = np.isnan(X)
    Y = Y[~np.any(nan_mask, axis=1)]
    X = X[~np.any(nan_mask, axis=1)]
    print(f'Done preparing the data. Number of obs.: {len(X)} and {X.shape[1]} columns.')
    
    # Tuning
    if tune == 'True':
        print('Started tuning... . ')
        start_time = time.time()
        # Hyper tuning of tree; define the parameter grid for grid search; for models with less
        # than 1000 observation we take into account a smaller forest.
        if len(X_train) <1000:
            n_est_lower = 1
            #n_est_lower = round(X_train.shape[0]/20)
            n_est_upper = round(X_train.shape[0]/25) 
            min_depth = 3
            max_depth = 5
        else:
            n_est_lower = 100
            n_est_upper = 150
            min_depth = 5
            max_depth = 8
                   
        param_grid = {
        'n_estimators': [n_est_lower, n_est_upper],
        'max_depth': [min_depth, max_depth],
        'min_samples_split': [5,  10]
        }

        # Initialize the Random Forest classifier
        rf_gs = RandomForestClassifier()

        # Perform grid search
        grid_search = GridSearchCV(estimator=rf_gs, param_grid=param_grid, cv=5)
        grid_search.fit(X_train, y_train)
        
        
        with open('/eos/jeodpp/data/projects/CLUM/Scripts/randomforest/hyperparameters.pkl', 'rb') as file:
            hyperparameters_dict = pickle.load(file)

        hyperparameters_nc = {'n_estimators':'','max_depth':'','min_samples_split':''}
        hyperparameters_dict[class_of_interest] = hyperparameters_nc    
        
        # Write to pickle file
        hyperparameters_dict[class_of_interest]['max_depth'] = grid_search.best_params_['max_depth']
        hyperparameters_dict[class_of_interest]['min_samples_split'] = grid_search.best_params_['min_samples_split']
        hyperparameters_dict[class_of_interest]['n_estimators'] = grid_search.best_params_['n_estimators']
        hyperparameters_dict[class_of_interest]['observations'] = len(X)
        print(f'Best Parameters:{hyperparameters_dict[class_of_interest]}.')
        
        with open('/eos/jeodpp/data/projects/CLUM/Scripts/randomforest/hyperparameters.pkl', 'wb') as file:
            pickle.dump(hyperparameters_dict, file)
        end_time = time.time()
        elapsed_time = round((end_time - start_time)/60)
        print(f'Finished tuning in {elapsed_time} minutes.')
    
    # 2. Train RF:
    # Load hyper parameters:
    if train == 'True':
        with open('/eos/jeodpp/data/projects/CLUM/Scripts/randomforest/hyperparameters.pkl', 'rb') as file:
            loaded_hyperparameters_dict = pickle.load(file)
        
        # Initialize the Random Forest classifier, the parameters chosen already come from the grid search below.
        rf = RandomForestClassifier(n_estimators=loaded_hyperparameters_dict[class_of_interest]['n_estimators'], 
                                max_depth=loaded_hyperparameters_dict[class_of_interest]['max_depth'],
                                min_samples_split=loaded_hyperparameters_dict[class_of_interest]['min_samples_split'],
                                n_jobs = 5, bootstrap=True)
        
        
        # Train the classifier
        rf.fit(X_train, y_train) 
        
        # Use predict_proba on test data
        classes = rf.classes_
        print("Class order:", classes)
             
        # 3. Save:
        source_calibration = f"/eos/jeodpp/data/projects/CLUM/Scripts/randomforest/model_calibration/calibrated_rf_model_{class_of_interest}.pkl"
        joblib.dump(rf, source_calibration)
    
        print('Done training the model.')
    
    # 4. Produce results:
    # 4.1.1:  AUC and recall:
    with open('/eos/jeodpp/data/projects/CLUM/Scripts/randomforest/hyperparameters.pkl', 'rb') as file:
            hyperparameters_dict = pickle.load(file)
    
    source_calibration = f"/eos/jeodpp/data/projects/CLUM/Scripts/randomforest/model_calibration/calibrated_rf_model_{class_of_interest}.pkl"
    loaded_rf = joblib.load(source_calibration)        
    
    if report == 'True':
        start_time = time.time()
        y_pred = loaded_rf.predict(X_test)
        y_pred_prob = loaded_rf.predict_proba(X_test)[:, 1]
        recall = recall_score(y_test, y_pred, average= 'weighted')
        auc_score = roc_auc_score(y_test, y_pred_prob)
        # Save to pickle file:
        hyperparameters_dict[class_of_interest]['auc_score'] = round(auc_score,2)
        hyperparameters_dict[class_of_interest]['recall'] = round(recall,2)

    # 4.1.2: Boyce_index:
        # Extract parameters for boyce calculation
        obs = y_test # y_test, y_pred
        obs = np.where(obs == class_of_interest, 1, 0)
        #print(np.unique(obs))
        #print(loaded_rf.classes_)
        
        y_pred_prob = loaded_rf.predict_proba(X_test)[:, 0]
        fit = y_pred_prob
        loc = f'/eos/jeodpp/data/projects/CLUM/Scripts/randomforest/boyce/boyce_index_{class_of_interest}.png'
       
        boyce_index = ecospat_boyce(fit, obs, nclass=0, window_w="default", res=100, plot = True, plot_save=loc)
        hyperparameters_dict[class_of_interest]['boyce'] = round(boyce_index,2)
    
    # Save Boyce and AUC:
        with open('/eos/jeodpp/data/projects/CLUM/Scripts/randomforest/hyperparameters.pkl', 'wb') as file:
             pickle.dump(hyperparameters_dict, file)
    
    # 4.2: confusion matrix:
        y_pred = loaded_rf.predict(X_test)
        cm = confusion_matrix(y_test, y_pred)
        conf_matrix_percent = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

        # Create a DataFrame from the confusion matrix percentage and observation number:
        cm_df = pd.DataFrame(conf_matrix_percent, index=np.unique(y_test), columns=np.unique(y_test))

        # Plot the confusion matrix using a heatmap
        plt.figure(figsize=(7, 7))
        sns.heatmap(cm_df, annot=True, cmap='Blues', linecolor='black', linewidths=1)
        plt.title('Confusion Matrix')
        plt.xlabel('Predicted')
        plt.ylabel('Actual')
        plt.savefig(f'/eos/jeodpp/data/projects/CLUM/Scripts/randomforest/confusion_matrix/confusion_matrix{class_of_interest}.png')
        plt.close()
        
        # 4.3: SHAP values:
        explainer = shap.TreeExplainer(loaded_rf)
        shap_values = explainer.shap_values(X_train, check_additivity=False)
        plt.figure(figsize=(7, 7))
        #fig = shap.dependence_plot(0, shap_values[0], X_train, interaction_index=None)
        fig = shap.summary_plot(shap_values[0], X_train, feature_names = covariate_names,  plot_size=0.2,
                          title = 'Feature impact on probability of being agriculture', show=False)
        plt.savefig(f'/eos/jeodpp/data/projects/CLUM/Scripts/randomforest/shap_values/shap_values{class_of_interest}.png')
        #plt.close()

        
        end_time = time.time()
        elapsed_time = round((end_time - start_time)/60)
        print(f'Finished reporting results in {elapsed_time} minutes.')


def main():
  train_rf_validate_save()
  
if __name__ == '__main__':
  main()
