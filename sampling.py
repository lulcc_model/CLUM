""" This Script entails a few functions, which are used to extract the X and Y variables for the 
binary classification model on land use transitions (random forest and logistic regression)
 authors: Erik Oudejans, september 2023, (Object Vision BV) 
          Ana Klinnert, september 2023, (JRC-Sevilla)"""


import geopandas as gpd
import rasterio as rio
import numpy as np
import pyarrow.parquet as pq
import time
from osgeo import ogr
from osgeo import osr
from rasterio.features import geometry_mask
from shapely.geometry import Point
from fiona.crs import from_epsg
import pyproj
import random
import csv

random.seed(42)

def create_landtransition_lookup_table(land_transitions_codes_filename:str):
     # create lookup table # "E:/SourceData/CLUM/Covariates/LULUC/Land_transitions/land_transitions_codes.txt"
    lut_from_to_lookup_table = np.ndarray(shape=(1515, 2), dtype="uint16")
    with open(land_transitions_codes_filename, "r") as csvfile:
        lut_codes_reader = csv.reader(csvfile, delimiter=";")
        next(lut_codes_reader) # skip header
        for row in lut_codes_reader:
            lut_code = int(row[-3])
            if lut_code == 0:
                continue
            lu_to    = int(row[-1])
            lu_from  = int(row[-2])
            lut_from_to_lookup_table[lut_code, 0] = lu_from
            lut_from_to_lookup_table[lut_code, 1] = lu_to
    return lut_from_to_lookup_table

random.seed(42)

# Helper functions:
def get_indices_of_interest(data, class_of_interest:int,year:str):
    '''
    Function returns indices of with True where selected biogeo and landuse transition class of interest is present.
    '''
    if year == 'all':
            lut = data["lu_lut_class"].to_numpy()
            indices = lut == class_of_interest
            true_count = (indices == True).sum()
            
            lut_lookup_table = create_landtransition_lookup_table("/eos/jeodpp/data/projects/CLUM/Scripts/clum/clum/landuse_transition_codes_table.txt")
            lu_from = int(lut_lookup_table[class_of_interest][0])*10000
            print(f'This is the class we are sampling the no change from {lu_from}.')
            indices_no_change = lut == lu_from
            true_indices = np.where(indices_no_change)
            random_true_indices = random.sample(list(true_indices[0]), true_count)
            indices_no_change = np.full(len(indices_no_change), False, dtype=bool)
            indices_no_change[random_true_indices] = True
            
            indices_comp = np.logical_or(indices, indices_no_change)
    
    if year == '06_12':
            lu0612 = data["LT_0612_LB"].to_numpy()
            indices = lu0612 == lu0612
            indices = lu0612 == class_of_interest
            true_count = (indices == True).sum()
            
            indices_np = lu0612 == -1
            true_indices = np.where(indices_np)
            random_true_indices = random.sample(list(true_indices[0]), true_count)
            indices_np = np.full(len(indices_np), False, dtype=bool)
            indices_np[random_true_indices] = True
            
            indices_comp = np.logical_or(indices, indices_np)
    
    if year == '12_18':
            lu1218 = data["LT_1218_LB"].to_numpy()
            indices = lu1218 == lu1218
            indices = lu1218 == class_of_interest
            true_count = (indices == True).sum()
            
            indices_np = lu1218 == 65534
            true_indices = np.where(indices_np)
            random_true_indices = random.sample(list(true_indices[0]), true_count)
            indices_np = np.full(len(indices_np), False, dtype=bool)
            indices_np[random_true_indices] = True
            
            indices_comp = np.logical_or(indices, indices_np)
 
    
    return indices_comp
    
def getCovariateAtIndices(covariate:str, data, indices, np_type=np.float32):
    return data[covariate].to_numpy().astype(np_type)[indices]
    
def getX (data, indices, excluded_covariates):

    '''
    Function returns covariates(X) at the provided indices.
    '''
    excluded_covariates = excluded_covariates
    num_observations = np.sum(indices)
    covariate_names = [covariate for covariate in data.column_names if covariate not in excluded_covariates]
    
    X_covariates = np.ndarray(shape=(num_observations,len(covariate_names)), dtype=np.float32)
    
    index = 0
    for covariate in data.column_names:
        if covariate in excluded_covariates:
            continue
        #print(f"covariate: {index} {covariate}")
        X_covariates[:,index] = getCovariateAtIndices(covariate, data, indices)
        index+=1

    return X_covariates

def getY(data, indices, year:str):
    '''
    Function returns target variable (Y) at the provided indices.
    '''
    if year == 'all':
        lut = data["lu_lut_class"].to_numpy()[indices]#.astype("uint16")
        
    if year == '12_18':
        lut = data["LT_1218_LB"].to_numpy()[indices].astype("uint16")
        
    if year == '06_12':
        lut = data["LT_0612_LB"].to_numpy()[indices].astype("uint16")
    
    result = lut
    
    return result




if __name__ == '__main__':
  main()
