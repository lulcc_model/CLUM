import geopandas as gpd
import rasterio as rio
import numpy as np
import pyarrow.parquet as pq
import time
from osgeo import gdal
from osgeo import ogr
from osgeo import osr
from rasterio.features import geometry_mask
from shapely.geometry import Point
from fiona.crs import from_epsg
import pyproj
import os


xres = 100
yres = -100

# define functions:
def get_epsg_3035_srs():
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(3035)
    srs.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
    return srs


def get_srs_from_dataset(ds, fallback_srs):
    proj = ds.GetProjection()

    if not proj:
        return fallback_srs

    if "Geocoding information not available" in proj:
        return fallback_srs

    srs_dataset = osr.SpatialReference(wkt=proj)
    srs_dataset.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)

    if not srs_dataset:
        return fallback_srs

    return srs_dataset

crs_3035 = get_epsg_3035_srs()

def warp_covariate_to_intermediate_tif(covariate_fn, intermediate_covariate_fn, bounds):    
    ds = gdal.Open(covariate_fn, gdal.GA_ReadOnly)
    no_data_value = ds.GetRasterBand(1).GetNoDataValue()

    value_type = ds.GetRasterBand(1).DataType
    has_negative_no_data_value_for_unsigned_type = not no_data_value or (no_data_value<0 and any(value_type==uvt for uvt in [1,2,4,12]))
    if not no_data_value or has_negative_no_data_value_for_unsigned_type:
        no_data_value = get_default_no_data_value_from_type(value_type)

    srs_in = get_srs_from_dataset(ds, crs_3035)
    
    kwargs = {"format": "GTiff", "outputBounds": (bounds['minx'],bounds['miny'],bounds['maxx'],bounds['maxy']),\
                "xRes":xres, "yRes":yres, "srcSRS":srs_in, "dstSRS":crs_3035,\
                "multithread":True, "creationOptions":["TILED=YES", "TFW=YES", "COMPRESS=LZW"],
                "dstNodata":no_data_value}
    ds = gdal.Warp(intermediate_covariate_fn, ds, **kwargs)

def rasterize_vector_layer(vector_dataset_in, raster_dataset_out, biogeographic_bounds):
    '''
    Function serves to rasterize biogeographic regions shapefile using the same settings
    as the same intermediate tiff.
    '''

    srs_epsg_3035 = get_epsg_3035_srs()
    
    head,_ = os.path.split(raster_dataset_out)
    if not os.path.exists(head):
        os.makedirs(head)
        
    if not os.path.exists(raster_dataset_out):
        if not biogeographic_bounds:
            biogeographic_bounds = get_minimum_spanning_extent_from_biogeographic_regions(config['clum_data_root'] + config['biogeographic_region'])

        kwargs = {"format": "GTiff", "outputBounds": (biogeographic_bounds["minx"],biogeographic_bounds["miny"],biogeographic_bounds["maxx"],biogeographic_bounds["maxy"]),\
              "xRes":xres, "yRes":yres, "outputSRS":srs_epsg_3035,\
              "creationOptions":["TILED=YES", "TFW=YES", "COMPRESS=LZW"],\
              "attribute":"PK_UID", "outputType":gdal.GDT_Byte, "noData":get_default_no_data_value_from_type(gdal.GDT_Byte)}
        gdal.Rasterize(raster_dataset_out, vector_dataset_in, **kwargs)
    else:
        print('Shapefile already rasterized.')

def get_unicode_type_code_from_numpy_type(numpy_type):
    return np.dtype(numpy_type).char

def open_raster_dataset_using_rio_with_filecheck(raster_dataset_filename:str):
    if not os.path.exists(raster_dataset_filename):
        raise (f"Error while opening raster dataset: {raster_dataset_filename}, file does not exist.")
    return rio.open(raster_dataset_filename)

def get_default_no_data_value_from_type(gdal_data_type):
    """
    key function which reads tiff files and warps it into desired format.
    
    enum  GDALDataType {
    GDT_Unknown = 0 , GDT_Byte = 1 , GDT_Int8 = 14 , GDT_UInt16 = 2 ,
    GDT_Int16 = 3 , GDT_UInt32 = 4 , GDT_Int32 = 5 , GDT_UInt64 = 12 ,
    GDT_Int64 = 13 , GDT_Float32 = 6 , GDT_Float64 = 7 , GDT_CInt16 = 8 ,
    GDT_CInt32 = 9 , GDT_CFloat32 = 10 , GDT_CFloat64 = 11 , GDT_TypeCount = 15
    }
    """
    if gdal_data_type == 1: # GDT_Byte
        return 128
    if gdal_data_type == 2: # GDT_UInt16
        return 65535
    if gdal_data_type == 3: # GDT_Int16
        return -32768
    if gdal_data_type == 6: # GDT_Float32 
        return -9999.0
    else: 
        i = 0
    
    return -9999.0

def convert_numpy_dtype_to_gdal_ogr_dtype(numpy_dtype):

    if (numpy_dtype == np.bool_):
        return (ogr.OFTInteger, ogr.OFSTBoolean)
    if (numpy_dtype == np.int16):
        return (ogr.OFTInteger, ogr.OFSTInt16)
    if (numpy_dtype == np.uint16):
        return (ogr.OFTInteger, ogr.OFSTNone)
    if (numpy_dtype == np.float32):
        return (ogr.OFTReal, ogr.OFSTFloat32)
    if (numpy_dtype == np.int64):
        return (ogr.OFTInteger64, ogr.OFSTNone)
    if (numpy_dtype == np.int_):
        return (ogr.OFTInteger, ogr.OFSTNone)
    if (numpy_dtype == np.str_ or numpy_dtype == np.string_):
        return (ogr.OFTString, ogr.OFSTNone)
    if (numpy_dtype == np.float64):
        return (ogr.OFTReal, ogr.OFSTNone)
    
    return (ogr.OFTInteger, ogr.OFSTNone)

def get_spatial_reference_of_epsg_from_code(code : int):
    return rio.crs.CRS.from_epsg(code)

def get_minimum_spanning_extent_from_biogeographic_regions(filename):
    # get minimum spanning extent based on biographic regions shapefile
    biogeographic_regions = gpd.read_file(filename)
    biogeographic_regions = biogeographic_regions[biogeographic_regions['PK_UID']!=10]
    minx, miny, maxx, maxy = biogeographic_regions.geometry.total_bounds
    biogeographic_bounds = {'minx': minx,'miny': miny,'maxx': maxx,'maxy': maxy}
    return  biogeographic_bounds

def export_sparse_to_tiff_dataset(template_raster_fn:str, out_raster_fn:str, xcoords, ycoords, sparse_data):
    # get parameters for out_raster from template_raster
    start_time = time.time()
    with rio.open(template_raster_fn, "r") as src:
        metainfo = src.profile
        bounds = src.bounds

    out_shape = (metainfo['height'], metainfo['width'])
    no_data_value = get_default_no_data_value_from_type(sparse_data.dtype)
    data_raster = np.full(out_shape, no_data_value, dtype=sparse_data.dtype) # todo: func for gdal nodata, not numpy, improve..
    row_indices = metainfo['height'] - ((ycoords-bounds.bottom) / abs(-100)).astype(np.uint32, copy=False)
    col_indices = ((xcoords-bounds.left) / 100).astype(np.uint32, copy=False)
    data_raster[row_indices, col_indices] = sparse_data

    metainfo['tiled']       = True
    metainfo['blockxsize']  = 256
    metainfo['blockysize']  = 256
    metainfo['compress']    = 'lzw'
    metainfo['num_threads'] = 'all_cpus'
    metainfo['nodata'] = no_data_value
    metainfo['dtype'] = sparse_data.dtype.name

    with rio.open(out_raster_fn, 'w', **metainfo) as dst:
        dst.write(data_raster, 1)

    print(f"Total time for writing: {time.time()-start_time}")
    return

def export_sparse_to_vector_dataset(out_vector_fn:str, layername, xcoords, ycoords, sparse_data):
    driver = ogr.GetDriverByName("ESRI Shapefile")
    ds = driver.CreateDataSource(out_vector_fn)
    srs =  osr.SpatialReference()
    srs.ImportFromEPSG(3035)
    layer = ds.CreateLayer(layername, srs, ogr.wkbPoint)
    ogr.OFTReal
    gdal_field_type, gdal_subfield_type = convert_numpy_dtype_to_gdal_ogr_dtype(sparse_data.dtype)
    z_field_def = ogr.FieldDefn("z", gdal_field_type)
    z_field_def.SetSubType(gdal_subfield_type)
    layer.CreateField(z_field_def)

    for i in range(xcoords.shape[0]):
        featureDefn = layer.GetLayerDefn()
        field_count = featureDefn.GetFieldCount()
        feature = ogr.Feature(featureDefn)
        point = ogr.Geometry(ogr.wkbPoint)
        point.AddPoint(float(xcoords[i]), float(ycoords[i]))
        feature.SetGeometry(point)
        feature.SetField("z", sparse_data[i].item())
        layer.CreateFeature(feature)
        feature = None

    # Save and close DataSource
    ds = None

    return

def zonal_stats(shapefile, sparse_subset, export = False, output_shapefile= None):
    '''
    shapefile_count: str location of either country or biogeographic shapefile
    sparse_subset: subset of sparse_data depeding on filtering of class of itnerest
    export and output_shapefile: optional only if you want to export shapefile

    Returns descending df of counts of occurences within {shapefile_count} geometry
    '''
    start_time = time.time()
    # Create point shapefile from x and y -coords:
    df_subset = sparse_subset[['xcoord','ycoord','lut1218']]
    df_subset.loc[:,'geometry'] = [Point(x, y) for x, y in zip(df_subset['xcoord'], df_subset['ycoord'])]
    gdf = gpd.GeoDataFrame(df_subset, geometry='geometry')
    gdf.crs = pyproj.CRS.from_epsg(3035)
    if export == True:
        gdf.to_file(output_shapefile, driver='ESRI Shapefile')
    
    # Read country shapefile and assign correct CRS
    country_gdf = gpd.read_file(shapefile)
    target_crs = pyproj.CRS.from_epsg(3035)
    country_gdf = country_gdf.to_crs(target_crs)

    # Spatial join to overlay the point shapefile with the country shapefile
    joined_gdf = gpd.sjoin(gdf, country_gdf, op='within')
    
    # Zonal statistics count:
    point_count_by_country = joined_gdf.groupby('NAME')[['geometry']].count().reset_index()
    point_count_by_country = point_count_by_country.rename(columns={'geometry':'count'})
    point_count_by_country = point_count_by_country.sort_values(by='count', ascending=False)
    
    print(f"Total time for writing: {time.time()-start_time}")
    return point_count_by_country


if __name__=="__main__":
    # test export_sparse_to_tiff_dataset
    test_template_raster = "E:/SourceData/CLUM/Cleaned/V2/eea_r_3035_100_m_clc12_V18_5_land_mask.tif"
    test_raster_out = "E:/SourceData/CLUM/test/"

    """ # TEST1
    with rio.open(test_template_raster, "r") as src:
        metainfo = src.profile
        bounds = src.bounds
    xcoords = np.arange(src.bounds.left, src.bounds.left+1000, 200, dtype=np.uint32)
    ycoords = np.arange(src.bounds.top, src.bounds.top-1000, -200, dtype=np.uint32)
    sparse_data = np.arange(src.bounds.left, src.bounds.left+1000, 200, dtype=np.uint32)

    export_sparse_to_tiff_dataset("E:/SourceData/CLUM/Cleaned/V2/eea_r_3035_100_m_clc12_V18_5_land_mask.tif", test_raster_out+"test.tiff", xcoords, ycoords, sparse_data)
    """

    """# TEST2
    clum_data_dir = "E:/SourceData/CLUM/"
    clum_data = pq.read_table(clum_data_dir + "Cleaned/V2/clum_calibration_data.parquet")
    xcoords = clum_data["xcoord"].to_numpy()
    ycoords = clum_data["ycoord"].to_numpy()
    sparse_data = clum_data["biogeo"].to_numpy()
    export_sparse_to_tiff_dataset(test_template_raster, test_raster_out+"test1.tiff", xcoords, ycoords, sparse_data)"""

    # TEST3
    clum_data_dir = "E:/SourceData/CLUM/"
    clum_data = pq.read_table(clum_data_dir + "Cleaned/V2/clum_calibration_data.parquet")
    xcoords = clum_data["xcoord"].to_numpy()
    ycoords = clum_data["ycoord"].to_numpy()
    sparse_data = clum_data["biogeo"].to_numpy()
    
    # subsample for testing
    n_samples = 1000
    sample_indices = np.random.randint(clum_data.num_rows, size=n_samples, dtype=int)
    xcoords     = xcoords[sample_indices]
    ycoords     = ycoords[sample_indices]
    sparse_data = sparse_data[sample_indices]
    export_sparse_to_vector_dataset(test_raster_out+"test1.shp", "test_sample", xcoords, ycoords, sparse_data)
